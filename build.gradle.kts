plugins {
    kotlin("multiplatform") version "1.9.10"
    id("maven-publish")
    signing
}

group = "cc.rbbl.kotlinx.oci"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        jvmToolchain(8)
        withJava()
        testRuns.named("test") {
            executionTask.configure {
                useJUnitPlatform()
            }
        }
    }
    js {
        browser {
            commonWebpackConfig {
                cssSupport {
                    enabled.set(true)
                }
            }
        }
    }
    val hostOs = System.getProperty("os.name")
    val isArm64 = System.getProperty("os.arch") == "aarch64"
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" && isArm64 -> macosArm64("native")
        hostOs == "Mac OS X" && !isArm64 -> macosX64("native")
        hostOs == "Linux" && isArm64 -> linuxArm64("native")
        hostOs == "Linux" && !isArm64 -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    
    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting
        val jsTest by getting
        val nativeMain by getting
        val nativeTest by getting
    }
}

val javadocJar by tasks.registering(Jar::class) {
    archiveClassifier.set("javadoc")
}

publishing {
    publications.withType<MavenPublication> {
        artifact(javadocJar)
        pom {
            name.set("OCI Distribution Kotlin")
            description.set("A Kotlin Multiplatform OCI Distribution Client")
            url.set("https://www.rbbl.cc/")
            licenses {
                license {
                    name.set("MIT License")
                    url.set("https://gitlab.com/rbbl/kotlinx/oci-distribution/-/blob/main/LICENSE")
                }
            }
            developers {
                developer {
                    id.set("rbbl-dev")
                    name.set("rbbl-dev")
                    email.set("dev@rbbl.cc")
                }
            }
            scm {
                url.set("https://gitlab.com/rbbl/kotlinx/oci-distribution/")
            }
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/48091795/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
        maven {
            name = "Snapshot"
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials(PasswordCredentials::class)
        }
        maven {
            name = "Central"
            url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials(PasswordCredentials::class)
        }
    }
}

signing {
    val signingKey: String? by project
    val signingPassword: String? by project
    useInMemoryPgpKeys(signingKey, signingPassword)
    setRequired({
        gradle.taskGraph.allTasks.filter { it.name.endsWith("ToCentralRepository") }.isNotEmpty()
    })
    sign(publishing.publications)
}
