# [oci-distribution](https://github.com/opencontainers/distribution-spec/blob/main/spec.md) Client - Kotlin Multiplatform

The initial Goal is to support a CLI Tools of mine which needs Authentication,
listing Tags and pulling and pushing Manifests. 

The long Term Goal is to fully support the [OCI Distribution Spec](https://github.com/opencontainers/distribution-spec/blob/main/spec.md) from the Client Side.