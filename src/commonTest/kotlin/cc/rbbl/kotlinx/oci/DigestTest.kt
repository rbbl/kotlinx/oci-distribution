package cc.rbbl.kotlinx.oci

import kotlin.test.Test

class DigestTest {
    @Test
    fun valid_digest_with_algorithm_sha256() {
        Digest("sha256:6c3c624b58dbbcd3c0dd82b4c53f04194d1247c6eebdaab7c610cf7d66709b3b")
    }

    @Test
    fun valid_digest_with_algorithm_multihash() {
        Digest("multihash+base58:QmRZxt2b1FVZPNqd8hsiykDL3TdBDeTSPX9Kv46HmX4Gx8")
    }

    @Test
    fun valid_digest_with_algorithm_sha256_base64() {
        Digest("sha256+b64u:LCa0a2j_xo_5m0U8HTBBNBNCLXBkg7-g-YpeiGJm564")
    }
}