package cc.rbbl.kotlinx.oci

import kotlin.test.Test
import kotlin.test.assertEquals

class ImageReferenceTest {
    @Test
    fun to_String_minimal() {
        assertEquals("docker.io/nginx:latest", ImageReference(repository = "nginx").toString())
    }

    @Test
    fun to_String_with_Registry_and_Tag() {
        assertEquals(
            "registry.gitlab.com/rbbl/genre-police:1.5.0",
            ImageReference("registry.gitlab.com", "rbbl/genre-police", "1.5.0").toString()
        )
    }

    @Test
    fun to_String_with_Registry_and_Digest() {
        assertEquals(
            "registry.gitlab.com/rbbl/genre-police@sha256:1b7c7e00cc48544e2be731f6890dfa54acd97e8c30d7addf139b971ee6662d64",
            ImageReference(
                "registry.gitlab.com",
                "rbbl/genre-police",
                digest = Digest("sha256:1b7c7e00cc48544e2be731f6890dfa54acd97e8c30d7addf139b971ee6662d64")
            ).toString()
        )
    }

    @Test
    fun parsing_minimal_String() {
        assertEquals(ImageReference(repository = "nginx"), imageReference("nginx"))
    }

    @Test
    fun parsing_String_with_Registry() {
        assertEquals(
            ImageReference("registry.gitlab.com", "rbbl/genre-police"),
            imageReference("registry.gitlab.com/rbbl/genre-police")
        )
    }

    @Test
    fun parsing_String_with_Registry_and_Tag() {
        assertEquals(
            ImageReference("registry.gitlab.com", "rbbl/genre-police", "1.5.0"),
            imageReference("registry.gitlab.com/rbbl/genre-police:1.5.0")
        )
    }

    @Test
    fun parsing_String_with_Registry_and_Digest() {
        assertEquals(
            ImageReference(
                "registry.gitlab.com",
                "rbbl/genre-police",
                digest = Digest("sha256:1b7c7e00cc48544e2be731f6890dfa54acd97e8c30d7addf139b971ee6662d64")
            ),
            imageReference("registry.gitlab.com/rbbl/genre-police@sha256:1b7c7e00cc48544e2be731f6890dfa54acd97e8c30d7addf139b971ee6662d64")
        )
    }
}