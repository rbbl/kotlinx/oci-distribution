package cc.rbbl.kotlinx.oci

data class ImageReference(
    val registry: String = "docker.io",
    val repository: String,
    val tag: String = "latest",
    val digest: Digest? = null
) {
    override fun toString(): String {
        return if (digest != null) {
            "$registry/$repository@$digest"
        } else {
            "$registry/$repository:$tag"
        }
    }
}

fun imageReference(imageTag: String): ImageReference {
    val registryAndPath = imageTag.split("@")[0].split(":")[0]
    val potentialRegistry = registryAndPath.split("/")[0]
    val registry = if (potentialRegistry.contains(".")) {
        potentialRegistry
    } else {
        "docker.io"
    }
    val repo = if (registry == potentialRegistry) {
        val parts = registryAndPath.split("/")
        parts.slice(1..<parts.size).joinToString("/")
    } else {
        registryAndPath
    }
    return if (imageTag.contains("@")) {
        ImageReference(registry, repo, digest = Digest(imageTag.split("@")[1]))
    } else {
        val repoAndTag = imageTag.split(":")
        return when (repoAndTag.size) {
            1 -> ImageReference(registry, repo, "latest")
            2 -> ImageReference(registry, repo, repoAndTag[1])
            else -> throw IllegalArgumentException("can't parse tag from '$imageTag'")
        }
    }
}

