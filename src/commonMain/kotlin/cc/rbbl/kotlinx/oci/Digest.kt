package cc.rbbl.kotlinx.oci

import kotlin.jvm.JvmInline

/**
 * is equivalent to the full Digest according to Spec. See [oci-image spec](https://github.com/opencontainers/image-spec/blob/v1.0.1/descriptor.md#digests)
 */
@JvmInline
value class Digest(val digest: String) {
    init {
        require(isDigest(digest)) {
            "Invalid Digest. Does not match Regex '${digestRegex.pattern}'"
        }
    }

    override fun toString(): String {
        return digest
    }
}

fun isDigest(string: String) = digestRegex.matches(string)

private val digestRegex = Regex("[a-z0-9]+(?:[+._-][a-z0-9]+)*:[a-zA-Z0-9=_-]+")